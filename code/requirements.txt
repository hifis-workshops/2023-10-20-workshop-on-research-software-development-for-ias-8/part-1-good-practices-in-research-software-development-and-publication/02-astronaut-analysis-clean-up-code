# Packages required to run the analysis script
pandas==1.2.4
matplotlib==3.4.2

# Packages required to run the test script
flake8==3.9.2
